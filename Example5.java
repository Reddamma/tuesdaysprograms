package Collection;
// Write a Java program to update specific array element by given element.
	import java.util.*;
	  public class Example5 {
	  public static void main(String[] args) {
	  ArrayList<String>al = new ArrayList<String>();
	  al.add("Red");
	  al.add("Green");
	  al.add("Orange");
	  al.add("White");
	  al.add("Black");
	  System.out.println(al);
	  al.set(2, "Yellow");  
	  System.out.println(al);
	 }
	}
